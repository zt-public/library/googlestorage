# @zettasoft/googlestorage

## คำอธิบาย
1. ใช้สำหรับอัปโหลดรูปขึ้น Google Storage เท่านั้น
2. รองรับทั้ง Publish และ Private
3. รองรับการใช้งานร่วมกับ Multer

## วิธีทดสอบ
```
    npm start
```

### วิธีการใช้งาน
1. สร้างโปรเจกต์ใน GCS
2. สร้าง service-google.json

   ![](https://storage.googleapis.com/bck_demo/step1.png)

   ![](https://storage.googleapis.com/bck_demo/step0.png)

   ![](https://storage.googleapis.com/bck_demo/step.png)

   ![](https://storage.googleapis.com/bck_demo/step2.png)

   ![](https://storage.googleapis.com/bck_demo/step3.png)
2. สร้าง .env (ดูตัวอย่าง .env.default)
3. ใช้งาน

```js
    const storage = new GoogleStorage('YOUR BUCKET NAME'); // GOOGLE_PROJECT_ID ใน .env
    const sourcePath = path.join(__dirname, `YOUR PATH`) // ที่เก็บ Folder รูปภาพ

    const items = await storage.onUploadToStorage(sourcePath, files, { public: true })
    const links = items.map((e) => storage.getPublicUrl(e.mediaPath)) // ดึง URL
```

### หมายเหตุ
- กรณีที่อยากใช้งาน Private
```js
    const items = await storage.onUploadToStorage(sourcePath, files)
    const links2 = await Promise.all(
        items2.map(async (e) => await storage.getPrivateUrl(e.name, config))
    )
```

### Options
                    
Option  | Default | Description
--------| ----- | -------------
public  | false | true or false