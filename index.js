const { Storage } = require('@google-cloud/storage');
const axios = require('axios')
const path = require('path');
const fs = require('fs')
const { promisify } = require('util');
require("./config_env");

class GoogleStorage {
    constructor(bucketName) {
        const { GOOGLE_PROJECT_ID, GOOGLE_KEY } = process.env;
        if (!GOOGLE_PROJECT_ID || !GOOGLE_KEY) {
            throw ('Please config GOOGLE_PROJECT_ID, GOOGLE_KEY')
        }
        this.storage = new Storage({
            projectId: GOOGLE_PROJECT_ID,
            keyFilename: GOOGLE_KEY
        })

        this.bucketName = bucketName;
        this.bucket = this.storage.bucket(bucketName);
    }

    getPublicUrl(fileName) {
        return `https://storage.googleapis.com/${fileName}`
    }

    async unlinkFile(sourcePath, files = []) {
        try {
            await Promise.all(files.map((f) => {
                const dir = `${sourcePath}/${f.filename}`;
                fs.unlink(dir, (error) => error && console.log(error));
                return true;
            }))
        } catch (error) {
            return []
        }
    }

    async onUploadToStorage(sourcePath, files = [], config = {}) {
        try {
            if (!sourcePath) {
                return { success: false, message: 'Please config sourcePath' }
            }
            let items = [];
            let resuts = [];
            await Promise.all(
                files.map(async (el, i) => {
                    const des = `${sourcePath}/${el.filename}`;
                    const res = (await this.bucket.upload(des, config))[0];
                    const { mediaLink, id, name, bucket } = res.metadata;
                    resuts.push({
                        url: res.publicUrl(),
                        mediaLink,
                        id,
                        name,
                        bucket,
                        mediaPath: `${bucket}/${name}`,
                        index: i
                    });
                    items = [...items, el.filename]
                })
            )
            await this.unlinkFile(sourcePath, files)
            return resuts.sort((a, b) => a.index - b.index);
        } catch (error) {
            console.log(`function onUploadToStorage: ${error}`);
            return []
        }
    }

    async getPrivateUrl(fileName, config = {}) {
        try {
            const date = new Date();
            date.setDate(date.getDate() + 7)
            const configure = {
                action: 'read',
                expires: date,
                ...config
            }
            let file = this.bucket.file(fileName);
            file.getSignedUrl = promisify(file.getSignedUrl);
            return await file.getSignedUrl(configure)
        } catch (error) {
            console.log(error)
            return ''
        }
    }

    async onUploadUrlToStorage({ url, filename = `${Date.now()}`, config = {} }) {
        try {
            if (!url) {
                return { success: false, message: 'Please config url' }
            }
            const res = await this.onDownloadFromURL(url, filename, config);
            return res
        } catch (error) {
            console.log(error)
        }
    }

    async onDownloadFromURL(url, filename, config) {
        try {
            const des = path.join(__dirname, `/assets/${filename}`);
            const response = await axios({ url, responseType: 'stream' });
            return new Promise((resolve, reject) => {
                response.data.pipe(fs.createWriteStream(des))
                    .on('error', err => {
                        console.log(err)
                        reject(err)
                    })
                    .on('finish', async () => {
                        const items = await this.onUploadToStorage(path.join(__dirname, `/assets/`), [{ filename }], config);
                        resolve(items[0])
                    })
            })
        } catch (error) {
            console.log(error);
        }
    }
}

module.exports = GoogleStorage;
