const path = require('path');
const GoogleStorage = require('.');

(async () => {
    try {
        console.log(`#### START STORAGE ####`)
        const storage = new GoogleStorage('bck_demo');
        // const sourcePath = path.join(__dirname, `./assets`)
        // const files = [{ filename: 'step.png' }, { filename: 'step0.png' },
        // { filename: 'step1.png' }, { filename: 'step2.png' }, { filename: 'step3.png' }]

        // /*** UPLOAD TO STORAGE */
        // const items = await storage.onUploadToStorage(sourcePath, files, { public: true })
        // console.log(`Result => `, items)
        // /*** items => [{ url: public link, desc: filename}] */

        // /** HOW TO GET PUBLIC URL */
        // const links = items.map((e) => storage.getPublicUrl(e.mediaPath))
        // console.log(`URL PUBLISH => `, links)

        // console.log('========================================================================================================================')
        // console.log('========================================================================================================================')

        // const files2 = [{ filename: 'test3.jpg' }]
        // const items2 = await storage.onUploadToStorage(sourcePath, files2)
        // console.log(`Result Private => `, items2)

        // const date = new Date()
        // const config = {
        //     expires: date.setDate(date.getDate() + 7)
        // }

        // const links2 = await Promise.all(
        //     items2.map(async (e) => await storage.getPrivateUrl(e.name, config))
        // )

        const items3 = await storage.onUploadUrlToStorage({
            url: 'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=5075966095809778&height=200&width=200&ext=1616152192&hash=AeSrAv3SSqwDo9o3u1o',
            filename: '123456.png',
            config: {
                public: true
            }
        })
        console.log(items3)
        console.log('!!! FINISH !!!')
    } catch (error) {
        console.log(error)
    }
})()